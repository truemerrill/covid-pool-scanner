/* @flow */
import React, {useState, useEffect} from 'react';
const _ = require('lodash');


export const slotState = {
  EMPTY: 0,
  FILLED: 1,
  BLANK: 2,
  NONE: 3
}

/**
 * Returns a string representation of the slotState
 * 
 * @param {number} s - slotState
//  */
// const slotStateName = (s: number): string => {
//   if (s === slotState.EMPTY) {
//     return 'EMPTY';
//   } else if (s === slotState.FILLED) {
//     return 'FILLED';
//   } else if (s === slotState.BLANK) {
//     return 'BLANK';
//   } else {
//     return 'NONE'
//   }
// }


export type slotType = {
  state: number,
  data: any,
  focus: boolean
};


function Slot({state, focus}: slotType) {

  const slotStyle = (f: boolean): string => f ? "slot slot-focused": "slot";
  const wellStyle = (s: number): string => {
    if (s === slotState.EMPTY) {
      return 'well well-empty';
    } else if (s === slotState.FILLED) {
      return 'well well-filled';
    } else if (s === slotState.BLANK) {
      return 'well well-blank';
    } else {
      return 'well well-none'
    }
  }
  
  return (
    <div className={slotStyle(focus)}>
      <div className={wellStyle(state)}/>
    </div>
  );
}


function Row({label, slots}: 
             {label: string, 
              slots: Array<slotType>}) {
  return (
    <div className="rack-row">
      <div className="label">{label}</div>
      {slots.map(s => Slot(s))}
    </div>
  );
}

export function Rack({slots, rows, cols}: 
                     {slots: Array<slotType>,
                      rows: number,
                      cols: number}) {

  const rackRowLabels = _.range(rows).map(row =>
    (10 + row).toString(36).toUpperCase());

  const rackColLabels = _.range(1, cols + 1).map(col =>
    (col).toString(36));
  const next = (array) => {
    let i = 0;
    return () => {
      let val = array[i];
      i += 1;
      return val;
    }
  }
  const nextRackRowLabel = next(rackRowLabels);

  const rackRows = [];
  for (let i = 0; i < slots.length; i += cols) {
    rackRows.push({
      label: nextRackRowLabel(),
      slots: slots.slice(i, i + cols)
    });
  }

  return (
    <div className="rack">
      <div className="rack-row">
        {rackColLabels.map(label => <div className="label">{label}</div>)}
      </div>
      {rackRows.map(r => <Row label={r.label} slots={r.slots}/>)}
    </div>
  );
}