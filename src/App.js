/* @flow */

import React, {useState, useEffect} from 'react';
import {Rack, slotState} from './Rack';
import type {slotType} from './Rack';
import './App.css';



// /**
//  * @description User type.  This matches a subset of the fields defined in the
//  *  `users` table defined in Aroon's app.  The user type maps a unique sample
//  *  identifier (i.e. a barcode) to participant data.
//  */

// type userType = {
//   gtId: string,             // GT ID number
//   email: string,            // Participant email address
//   phone: string,            // Participant phone number
//   firstName: string | void, // Participant first name, NULLABLE
//   lastName: string | void,  // Participant last name, NULLABLE
//   sampleId: string,         // Sample ID (i.e. barcode), UNIQUE
//   createdAt: number,        // UTC timestamp, seconds since Unix epoch
// }

// // Alias for userType
// type sampleType = userType;


// const tubeStatus = {
//   EMPTY = 0,
//   FILLED = 1
// };

// type tubeType = {
//   status: number,
//   focus: boolean,
//   handleClick: () => void
// };

// /**
//  * @description Component rendering a single tube in the Integra rack
//  * 
//  * @param {status} whether the rack location is empty or filled
//  * @param {focus} whether the rack location has focus
//  * @param {handleClick}
//  */
// function Tube({status, focus, handleClick}: tubeType) {

//   // Assigns CSS classes for styling
//   function tubeClass(status, focus) {
//     const statusClass = (status) => {
//       if (status == tubeStatus.EMPTY) {
//         return "tube-empty";
//       } else {
//         return "tube-filled";
//       }
//     }
//     const focusClass = (focus) => focus ? "tube-focus" : "";
//     return ["tube", statusClass(status), focusClass(focus)].join(" ");
//   }

//   return (
//     <div
//       className={tubeClass(status, focus)}
//       onClick={handleClick}
//     />
//   );

// }

// function Row(tubes: Array<tubeType>) {
//   return (
//     <div className="rack-row">
//       {tubes.map(tube => Tube(tube))}
//     </div>
//   )
// }


// function Rack({numRows, numCols}) {

//   [focus, setFocus] = useState(focusState(0));
//   [status, setStatus] = useState(new Array(numRows * numCols).fill(tubeStatus.EMPTY));

//   /**
//    * @description A helper function to create a new `focus` state.  Returns an
//    *  array that is false everywhere except for at `index` which is set to true.
//    */
//   function focusState(index) {
//     const focusState = new Array(numRows * numCols).fill(false);
//     focusState[index] = true;
//     return focusState;
//   }

//   /**
//    * @description First-order function to create the handle click callbacks
//    */
//   function createHandleClick(index) {
//     return () => setFocus(focusState(index));
//   }

//   function createTube(index: number): tubeType {
//     return {
//       status: status[index],
//       focus: focus[index],
//       handleClick: createHandleClick(index)
//     };
//   }

//   function createRows() {
//     const rows = [];
//     let index = 0;
//     for (let i = 0; i < numRows; i++) {
//       let row = [];
//       for (let j = 0; j < numCols; j++) {
//         row.push(createTube(index));
//         index += 1;
//       }
//       rows.push(row);
//     }
//     return rows;
//   }

//   return (
//     <div className="rack">
//       {createRows().map(row => Row(row))}
//     </div>
//   );
// }


function App() {

  const slots = [
    {
      state: slotState.EMPTY,
      data: null,
      focus: true
    },
    {
      state: slotState.EMPTY,
      data: null,
      focus: false
    },
    {
      state: slotState.EMPTY,
      data: null,
      focus: false
    },
    {
      state: slotState.FILLED,
      data: {
        barcode: "12341234"
      },
      focus: false
    },
    {
      state: slotState.BLANK,
      data: null,
      focus: false
    },
    {
      state: slotState.NONE,
      data: null,
      focus: false
    },
    {
      state: slotState.EMPTY,
      data: null,
      focus: false
    },
    {
      state: slotState.EMPTY,
      data: null,
      focus: false
    },
    {
      state: slotState.EMPTY,
      data: null,
      focus: false
    },
    {
      state: slotState.FILLED,
      data: {
        barcode: "12341234"
      },
      focus: false
    },
    {
      state: slotState.BLANK,
      data: null,
      focus: false
    },
    {
      state: slotState.NONE,
      data: null,
      focus: false
    },
    {
      state: slotState.EMPTY,
      data: null,
      focus: false
    },
    {
      state: slotState.EMPTY,
      data: null,
      focus: false
    },
    {
      state: slotState.EMPTY,
      data: null,
      focus: false
    },
    {
      state: slotState.FILLED,
      data: {
        barcode: "12341234"
      },
      focus: false
    },
    {
      state: slotState.BLANK,
      data: null,
      focus: false
    },
    {
      state: slotState.NONE,
      data: null,
      focus: false
    },
    {
      state: slotState.EMPTY,
      data: null,
      focus: false
    },
    {
      state: slotState.EMPTY,
      data: null,
      focus: false
    },
    {
      state: slotState.EMPTY,
      data: null,
      focus: false
    },
    {
      state: slotState.FILLED,
      data: {
        barcode: "12341234"
      },
      focus: false
    },
    {
      state: slotState.BLANK,
      data: null,
      focus: false
    },
    {
      state: slotState.NONE,
      data: null,
      focus: false
    },
    {
      state: slotState.EMPTY,
      data: null,
      focus: false
    },
    {
      state: slotState.EMPTY,
      data: null,
      focus: false
    },
    {
      state: slotState.EMPTY,
      data: null,
      focus: false
    },
    {
      state: slotState.FILLED,
      data: {
        barcode: "12341234"
      },
      focus: false
    },
    {
      state: slotState.BLANK,
      data: null,
      focus: false
    },
    {
      state: slotState.NONE,
      data: null,
      focus: false
    },
    {
      state: slotState.EMPTY,
      data: null,
      focus: false
    },
    {
      state: slotState.EMPTY,
      data: null,
      focus: false
    },
    {
      state: slotState.EMPTY,
      data: null,
      focus: false
    },
    {
      state: slotState.FILLED,
      data: {
        barcode: "12341234"
      },
      focus: false
    },
    {
      state: slotState.BLANK,
      data: null,
      focus: false
    },
    {
      state: slotState.NONE,
      data: null,
      focus: false
    },
    {
      state: slotState.EMPTY,
      data: null,
      focus: false
    },
    {
      state: slotState.EMPTY,
      data: null,
      focus: false
    },
    {
      state: slotState.EMPTY,
      data: null,
      focus: false
    },
    {
      state: slotState.FILLED,
      data: {
        barcode: "12341234"
      },
      focus: false
    },
    {
      state: slotState.BLANK,
      data: null,
      focus: false
    },
    {
      state: slotState.NONE,
      data: null,
      focus: false
    },
    {
      state: slotState.EMPTY,
      data: null,
      focus: false
    },
    {
      state: slotState.EMPTY,
      data: null,
      focus: false
    },
    {
      state: slotState.EMPTY,
      data: null,
      focus: false
    },
    {
      state: slotState.FILLED,
      data: {
        barcode: "12341234"
      },
      focus: false
    },
    {
      state: slotState.BLANK,
      data: null,
      focus: false
    },
    {
      state: slotState.NONE,
      data: null,
      focus: false
    }
  ]

  return (
    <div className="App">
      <Rack rows={8} cols={6} slots={slots}/>
    </div>
  );
}

export default App;
